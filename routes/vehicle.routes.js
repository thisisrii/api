const router = require("express").Router();
const Vehicle = require("../model/vehicle.model");
const { vehicleValidation } = require("../validation");

router.get("/", async (req, res) => {

    console.log('request', req)

    const vehicles = await Vehicle.find({ user: req.user.email })

    try {
        res.json({ error: null, data: vehicles });
    } catch (error) {
        res.status(400).json({ error })
    }
});

router.post("/", async (req, res) => {

    const { error } = vehicleValidation(req.body);

    if (error)
        return res.status(400).json({ error: error.details[0].message });


    const vehicle = new Vehicle({
        make: req.body.make,
        model: req.body.model,
        description: req.body.description,
        user: req.user.email
    });

    try {
        const savedVehicle = await vehicle.save();
        res.json({ error: null, data: savedVehicle._id });
    } catch (error) {
        res.status(400).json({ error })
    }

})

module.exports = router;