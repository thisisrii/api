const router = require("express").Router();
const User = require("../model/user.model");
const { registerValidation, loginValidation } = require("../validation")
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")

router.post("/register", async (req, res) => {

    const { error } = registerValidation(req.body);

    if (error)
        return res.status(400).json({ error: error.details[0].message })

    const emailExist = await User.findOne({ email: req.body.email });

    if (emailExist)
        return res.status(400).json({ error: "Email already exists." })

    const salt = await bcrypt.genSalt(10);
    const password = await bcrypt.hash(req.body.password, salt);

    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: password,
    });

    try {
        const savedUser = await user.save();
        res.json({ error: null, data: savedUser._id });
    } catch (error) {
        res.status(400).json({ error })
    }
});

router.post("/login", async (req, res) => {

    const { error } = loginValidation(req.body);

    if (error)
        return res.status(400).json({ error: error.details[0].message });

    const user = await User.findOne({ email: req.body.email });

    if (!user)
        return res.status(404).json({ error: "User does not exist" })

    const validPassword = await bcrypt.compare(req.body.password, user.password);

    if (!validPassword)
        return res.status(400).json({ error: "Password is incorrect." });

    const token = jwt.sign(
        {
            name: user.name,
            email: user.email,
            id: user._id,
        },
        process.env.TOKEN_SECRET
    );

    res.header("auth-token", token).json({
        error: null,
        data: {
            token
        }
    })
})

module.exports = router;