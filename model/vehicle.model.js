const mongoose = require("mongoose");

const vehicleSchema = new mongoose.Schema({
    make: {
        type: String,
        required: true,
        min: 3,
        max: 255,
    },
    model: {
        type: String,
        required: true,
        min: 3,
        max: 255,
    },
    description: {
        type: String,
        required: false,
        min: 6,
        max: 1024,
    },
    inputDate: {
        type: Date,
        default: Date.now,
    },
    user: {
        type: String,
        min: 6,
        max: 1024,
    }
})

module.exports = mongoose.model("Vehicle", vehicleSchema);