// validation

const Joi = require("@hapi/joi");

const registerValidation = (data) => {
    const schema = Joi.object({
        name: Joi.string().min(3).max(255).required(),
        email: Joi.string().min(6).max(255).required().email(),
        password: Joi.string().min(6).max(1024).required(),
    });
    return schema.validate(data);
}


const loginValidation = (data) => {
    const schema = Joi.object({
        email: Joi.string().min(6).max(255).required().email(),
        password: Joi.string().min(6).max(1024).required(),
    })
    return schema.validate(data);
}

const vehicleValidation = (data) => {
    const schema = Joi.object({
        make: Joi.string().min(3).max(255).required(),
        model: Joi.string().min(3).max(255).required(),
        description: Joi.string().min(6).max(1024),
        user: Joi.string().min(6).max(1024)
    })
    return schema.validate(data);
}

module.exports = {
    registerValidation,
    loginValidation,
    vehicleValidation,
}