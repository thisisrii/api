const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const cors = require("cors");

const app = express();

dotenv.config();

app.use(cors());

mongoose.connect(process.env.DB_CONNECT, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}, () => console.log("connected to db..."))

const authRoutes = require("./routes/auth.routes");
const vehicleRoutes = require("./routes/vehicle.routes");
const verifyToken = require("./routes/validate-token");

app.use(express.json());

app.use("/api/user", authRoutes);
app.use("/api/vehicle", verifyToken, vehicleRoutes);

app.listen(3001, () => console.log("server is running..."));